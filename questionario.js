export default [

    // Equações Lineares

    {
      question: "2x - 5 = 11",
      answers: [
        { option: "8", correct: true },
        { option: "7", correct: false },
        { option: "9", correct: false },
        { option: "10", correct: false }
      ],
      difficult: 1
    },

    {
      question: "3y + 4 = 25",
      answers: [
        { option: "8", correct: false },
        { option: "7", correct: false },
        { option: "7.5", correct: true },
        { option: "6", correct: false }
      ],
      difficult: 1
    },

    {
      question: "4a - 8 = 12",
      answers: [
        { option: "5", correct: false },
        { option: "6", correct: false },
        { option: "8", correct: true },
        { option: "7", correct: false }
      ],
      difficult: 1
    },

    {
      question: "x + 7 = 15",
      answers: [
        { option: "8", correct: true },
        { option: "9", correct: false },
        { option: "7", correct: false },
        { option: "10", correct: false }
      ],
      difficult: 1
    },

    {
      question: "6y + 10 = 34",
      answers: [
        { option: "4", correct: false },
        { option: "5", correct: false },
        { option: "6", correct: true },
        { option: "7", correct: false }
      ],
      difficult: 1
    },

    {
      question: "2x - 3 = 7",
      answers: [
        { option: "4", correct: true },
        { option: "5", correct: false },
        { option: "6", correct: false },
        { option: "3", correct: false }
      ],
      difficult: 1
    },

    {
      question: "5a + 9 = 24",
      answers: [
        { option: "3", correct: false },
        { option: "4", correct: true },
        { option: "5", correct: false },
        { option: "6", correct: false }
      ],
      difficult: 1
    },

    {
      question: "x + 5 = 10",
      answers: [
        { option: "5", correct: true },
        { option: "6", correct: false },
        { option: "7", correct: false },
        { option: "4", correct: false }
      ],
      difficult: 1
    },

    {
      question: "4y - 6 = 10",
      answers: [
        { option: "4", correct: false },
        { option: "3", correct: false },
        { option: "2", correct: false },
        { option: "5", correct: true }
      ],
      difficult: 1
    },

    {
      question: "3a + 2 = 17",
      answers: [
        { option: "6", correct: false },
        { option: "5", correct: false },
        { option: "7", correct: false },
        { option: "15", correct: true }
      ],
      difficult: 1
    },

    {
      question: "2x + 3 = 9",
      answers: [
        { option: "3", correct: true },
        { option: "6", correct: false },
        { option: "4", correct: false },
        { option: "2", correct: false }
      ],
      difficult: 1
    },

    {
      question: "4y - 7 = 13",
      answers: [
        { option: "12", correct: false },
        { option: "13", correct: false },
        { option: "17", correct: false },
        { option: "20", correct: true }
      ],
      difficult: 1
    },

    {
      question: "3a + 5 = 14",
      answers: [
        { option: "1", correct: false },
        { option: "2", correct: false },
        { option: "3", correct: true },
        { option: "4", correct: false }
      ],
      difficult: 1
    },

    {
      question: "x + 8 = 20",
      answers: [
        { option: "11", correct: false },
        { option: "12", correct: true },
        { option: "15", correct: false },
        { option: "14", correct: false }
      ],
      difficult: 1
    },

    {
      question: "6y - 4= 14",
      answers: [
        { option: "8", correct: false },
        { option: "5", correct: false },
        { option: "3", correct: true },
        { option: "7", correct: false }
      ],
      difficult: 1
    },

    {
      question: "2x - 9 = 5",
      answers: [
        { option: "6", correct: false },
        { option: "5", correct: false },
        { option: "7", correct: true },
        { option: "10", correct: false }
      ],
      difficult: 1
    },

    {
      question: "4a + 6 = 26",
      answers: [
        { option: "4", correct: false },
        { option: "3", correct: false },
        { option: "1", correct: false },
        { option: "5", correct: true }
      ],
      difficult: 1
    },

    {
      question: "x + 12 = 18",
      answers: [
        { option: "6", correct: true },
        { option: "9", correct: false },
        { option: "1", correct: false },
        { option: "8", correct: false }
      ],
      difficult: 1
    },

    {
      question: "5y - 8 = 17",
      answers: [
        { option: "4", correct: false },
        { option: "8", correct: false },
        { option: "9", correct: false },
        { option: "5", correct: true }
      ],
      difficult: 1
    },

    {
      question: "3a + 7 = 22",
      answers: [
        { option: "2", correct: false },
        { option: "5", correct: true },
        { option: "4", correct: false },
        { option: "6", correct: false }
      ],
      difficult: 1
    },

    {
      question: "2x + 5 = 17",
      answers: [
        { option: "6", correct: false },
        { option: "7", correct: false },
        { option: "8", correct: true },
        { option: "9", correct: false }
      ],
      difficult: 1
    },
  
    {
      question: "3y - 4 = 10",
      answers: [
        { option: "6", correct: true },
        { option: "7", correct: false },
        { option: "8", correct: false },
        { option: "9", correct: false }
      ],
      difficult: 1
    },
  
    {
      question: "4a + 7 = 31",
      answers: [
        { option: "6", correct: false },
        { option: "8", correct: false },
        { option: "9", correct: true },
        { option: "10", correct: false }
      ],
      difficult: 1
    },
  
    {
      question: "x - 6 = 13",
      answers: [
        { option: "18", correct: false },
        { option: "19", correct: false },
        { option: "20", correct: true },
        { option: "21", correct: false }
      ],
      difficult: 1
    },
  
    {
      question: "6y + 8 = 44",
      answers: [
        { option: "6", correct: false },
        { option: "7", correct: false },
        { option: "8", correct: false },
        { option: "9", correct: true }
      ],
      difficult: 1
    },

    {
      question: "3x + 5 = 14",
      answers: [
        { option: "3", correct: false },
        { option: "2", correct: false },
        { option: "4", correct: true },
        { option: "5", correct: false }
      ],
      difficult: 1
    },
  
    {
      question: "2y - 8 = 10",
      answers: [
        { option: "5", correct: true },
        { option: "4", correct: false },
        { option: "6", correct: false },
        { option: "7", correct: false }
      ],
      difficult: 1
    },
  
    {
      question: "4a + 7 = 31",
      answers: [
        { option: "6", correct: false },
        { option: "8", correct: false },
        { option: "5", correct: false },
        { option: "9", correct: true }
      ],
      difficult: 1
    },
  
    {
      question: "x + 6 = 12",
      answers: [
        { option: "4", correct: false },
        { option: "5", correct: true },
        { option: "6", correct: false },
        { option: "7", correct: false }
      ],
      difficult: 1
    },
  
    {
      question: "3y - 9 = 18",
      answers: [
        { option: "7", correct: false },
        { option: "9", correct: false },
        { option: "8", correct: true },
        { option: "10", correct: false }
      ],
      difficult: 1
    },
  
    {
      question: "5x - 3 = 22",
      answers: [
        { option: "7", correct: false },
        { option: "6", correct: false },
        { option: "5", correct: false },
        { option: "9", correct: true }
      ],
      difficult: 1
    },
  
    {
      question: "4y + 12 = 32",
      answers: [
        { option: "5", correct: false },
        { option: "6", correct: false },
        { option: "7", correct: false },
        { option: "8", correct: true }
      ],
      difficult: 1
    },


    // Equações Lineares com Parênteses

    {
      question: "9(2x + 10) = 189",
      answers: [
        { option: "9", correct: false },
        { option: "10", correct: true },
        { option: "11", correct: false },
        { option: "12", correct: false }
      ],
      difficult: 2
    },
  
    {
      question: "5(3y - 2) = 45",
      answers: [
        { option: "8", correct: false },
        { option: "9", correct: true },
        { option: "10", correct: false },
        { option: "11", correct: false }
      ],
      difficult: 2
    },

    {
      question: "2(x + 4) = 18",
      answers: [
        { option: "6", correct: false },
        { option: "5", correct: true },
        { option: "4", correct: false },
        { option: "7", correct: false }
      ],
      difficult: 2,
    },

    {
      question: "3(3y - 2) = 21",
      answers: [
        { option: "3", correct: false },
        { option: "6", correct: true },
        { option: "4", correct: false },
        { option: "2", correct: false }
      ],
      difficult: 2
    },

    {
      question: "4(2a + 5) = 48",
      answers: [
        { option: "3", correct: false },
        { option: "6", correct: false },
        { option: "4", correct: true },
        { option: "2", correct: false }
      ],
      difficult: 2
    },

    {
      question: "5(2x - 3) = 45",
      answers: [
        { option: "7", correct: false },
        { option: "6", correct: false },
        { option: "8", correct: false },
        { option: "9", correct: true }
      ],
      difficult: 2
    },

    {
      question: "6(4y + 2) = 42",
      answers: [
        { option: "6", correct: false },
        { option: "5", correct: true },
        { option: "4", correct: false },
        { option: "7", correct: false }
      ],
      difficult: 2
    },
  
    {
      question: "7(4a + 3) = 91",
      answers: [
        { option: "11", correct: false },
        { option: "12", correct: false },
        { option: "13", correct: true },
        { option: "14", correct: false }
      ],
      difficult: 2
    },

    {
      question: "2(x + 5) = 24",
      answers: [
        { option: "7", correct: false },
        { option: "8", correct: true },
        { option: "6", correct: false },
        { option: "5", correct: false }
      ],
      difficult: 2
    },
  
    {
      question: "3(2y - 3) = 33",
      answers: [
        { option: "9", correct: false },
        { option: "10", correct: true },
        { option: "8", correct: false },
        { option: "7", correct: false }
      ],
      difficult: 2
    },
  
    {
      question: "4(3a + 4) = 44",
      answers: [
        { option: "6", correct: false },
        { option: "8", correct: false },
        { option: "9", correct: true },
        { option: "7", correct: false }
      ],
      difficult: 2
    },
  
    {
      question: "5(2x - 2) = 35",
      answers: [
        { option: "6.5", correct: false },
        { option: "7.5", correct: true },
        { option: "8.5", correct: false },
        { option: "9.5", correct: false }
      ],
      difficult: 2
    },
  
    {
      question: "3(2x + 7) = 51",
      answers: [
        { option: "12", correct: false },
        { option: "13", correct: false },
        { option: "14", correct: false },
        { option: "15", correct: true }
      ],
      difficult: 2
    },
  
    {
      question: "10(3x + 2) = 100",
      answers: [
        { option: "8", correct: false },
        { option: "9", correct: false },
        { option: "10", correct: false },
        { option: "11", correct: true }
      ],
      difficult: 2
    },
  
    {
      question: "8(4 + z) = 120",
      answers: [
        { option: "13", correct: false },
        { option: "14", correct: false },
        { option: "15", correct: false },
        { option: "16", correct: true }
      ],
      difficult: 2
    },
  
    {
      question: "2(5y - 3) = 32",
      answers: [
        { option: "9", correct: false },
        { option: "10", correct: true },
        { option: "11", correct: false },
        { option: "12", correct: false }
      ],
      difficult: 2
    },
  
    {
      question: "7(x + 6) = 49",
      answers: [
        { option: "5", correct: false },
        { option: "6", correct: false },
        { option: "7", correct: false },
        { option: "8", correct: true }
      ],
      difficult: 2
    },
  
    {
      question: "3(x - 4) = 27",
      answers: [
        { option: "8", correct: true },
        { option: "9", correct: false },
        { option: "10", correct: false },
        { option: "11", correct: false }
      ],
      difficult: 2
    },

    {
      question: "2(x + 3) = 14",
      answers: [
        { option: "6", correct: false },
        { option: "5", correct: false },
        { option: "4", correct: true },
        { option: "9", correct: false }
      ],
      difficult: 2
    },

    {
      question: "3(2y - 5) = 21",
      answers: [
        { option: "3", correct: false },
        { option: "6", correct: true },
        { option: "4", correct: false },
        { option: "2", correct: false }
      ],
      difficult: 2
    },

    {
      question: "4(3a + 2) = 32",
      answers: [
        { option: "3", correct: false },
        { option: "6", correct: false },
        { option: "4", correct: false },
        { option: "2", correct: true }
      ],
      difficult: 2
    },

    {
      question: "5(2x - 4) = 35",
      answers: [
        { option: "11/5", correct: true },
        { option: "31/10", correct: false },
        { option: "24/5", correct: false },
        { option: "12/5", correct: false }
      ],
      difficult: 2
    },
    
    {
      question: "9(2x + 15) = 153",
      answers: [
        { option: "3", correct: false },
        { option: "2", correct: false },
        { option: "1", correct: true },
        { option: "4", correct: false }
      ],
      difficult: 2
    },

    {
      question: "10(5x + 20) = 150",
      answers: [
        { option: "8", correct: false },
        { option: "6", correct: false },
        { option: "7", correct: true },
        { option: "9", correct: false }
      ],
      difficult: 2
    },

    {
      question: "11(30 + z) = 143",
      answers: [
        { option: "-18", correct: false },
        { option: "17", correct: false },
        { option: "-17", correct: true },
        { option: "18", correct: false }
      ],
      difficult: 2
    },

    {
      question: "2(4y + 3) = 22",
      answers: [
        { option: "3", correct: false },
        { option: "6", correct: false },
        { option: "2", correct: true },
        { option: "4", correct: false }
      ],
      difficult: 2
    },

    {
      question: "7(x + 25) = 14",
      answers: [
        { option: "-23", correct: true },
        { option: "23", correct: false },
        { option: "22", correct: false },
        { option: "-21", correct: false }
      ],
      difficult: 2
    },

    {
      question: "2(x - 15) = 40",
      answers: [
        { option: "5", correct: true },
        { option: "4", correct: false },
        { option: "3", correct: false },
        { option: "2", correct: false }
      ],
      difficult: 2
    },

    {
      question: "3(20 - x) = 45",
      answers: [
        { option: "5", correct: true },
        { option: "4", correct: false },
        { option: "6", correct: false },
        { option: "7", correct: false }
      ],
      difficult: 2
    },

    {
      question: "4(2x - 18) = 16",
      answers: [
        { option: "11", correct: true },
        { option: "12", correct: false },
        { option: "13", correct: false },
        { option: "14", correct: false }
      ],
      difficult: 2
    },

    {
      question: "5(x + 25) = 125",
      answers: [
        { option: "0", correct: true },
        { option: "1", correct: false },
        { option: "2", correct: false },
        { option: "-1", correct: false }
      ],
      difficult: 2
    },

    // C/ Frações

    {
      question: "(2/3)x + 1 = 3",
      answers: [
        { option: "2", correct: false },
        { option: "3/2", correct: false },
        { option: "4/3", correct: true },
        { option: "1/2", correct: false }
      ],
      difficult: 3
    },
  
    {
      question: "(3/5)y - (1/2) = 2",
      answers: [
        { option: "7/5", correct: false },
        { option: "3/2", correct: true },
        { option: "5/3", correct: false },
        { option: "2/5", correct: false }
      ],
      difficult: 3
    },
  
    {
      question: "(1/4)a + (1/8) = 3/2",
      answers: [
        { option: "3/4", correct: false },
        { option: "7/8", correct: false },
        { option: "5/4", correct: true },
        { option: "1/2", correct: false }
      ],
      difficult: 3
    },

    {
      question: "(1/2)x + 3 = 7",
      answers: [
        { option: "6", correct: true },
        { option: "7", correct: false },
        { option: "8", correct: false },
        { option: "9", correct: false }
      ],
      difficult: 3
    },
  
    {
      question: "(3/4)y - 2 = 10",
      answers: [
        { option: "6", correct: true },
        { option: "7", correct: false },
        { option: "8", correct: false },
        { option: "9", correct: false }
      ],
      difficult: 3
    },
  
    {
      question: "(2/3)a + 4 = 14",
      answers: [
        { option: "7", correct: false },
        { option: "8", correct: false },
        { option: "9", correct: true },
        { option: "10", correct: false }
      ],
      difficult: 3
    },
  
    {
      question: "(3/5)x - 1 = 7",
      answers: [
        { option: "5", correct: true },
        { option: "6", correct: false },
        { option: "7", correct: false },
        { option: "8", correct: false }
      ],
      difficult: 3
    },
  
    {
      question: "(1/4)y + 5 = 8",
      answers: [
        { option: "3", correct: true },
        { option: "4", correct: false },
        { option: "5", correct: false },
        { option: "6", correct: false }
      ],
      difficult: 3
    },

    {
      question: "(3/4)a - 1 = 5",
      answers: [
        { option: "2", correct: false },
        { option: "6", correct: true },
        { option: "5", correct: false },
        { option: "3", correct: false }
      ],
      difficult: 3
    },

    {
      question: "(1/2)x + (3/4) = 4",
      answers: [
        { option: "26/4", correct: true },
        { option: "31/4", correct: false },
        { option: "23/5", correct: false },
        { option: "21/2", correct: false }
      ],
      difficult: 3
    },

    {
      question: "(3/4)x + 7 = 10",
      answers: [
        { option: "1", correct: true },
        { option: "3", correct: false },
        { option: "6", correct: false },
        { option: "7", correct: false }
      ],
      difficult: 3
    },

    {
      question: "(5/6)x - (3/3) = 8",
      answers: [
        { option: "52/5", correct: true },
        { option: "34/5", correct: false },
        { option: "23/5", correct: false },
        { option: "56/5", correct: false }
      ],
      difficult: 3
    },

    {
      question: "(2/9)x + (1/3) = (5/6)",
      answers: [
        { option: "3/2", correct: true },
        { option: "2/3", correct: false },
        { option: "5/4", correct: false },
        { option: "5/4", correct: false }
      ],
      difficult: 3
    },

    {
      question: "(4/5)x + (1/10) = (3/2)",
      answers: [
        { option: "7/4", correct: true },
        { option: "31/4", correct: false },
        { option: "26/5", correct: false },
        { option: "24/5", correct: false }
      ],
      difficult: 3
    },

    {
      question: "(2/15)x - (1/3) = (1/5)",
      answers: [
        { option: "12/5", correct: true },
        { option: "11/5", correct: false },
        { option: "10/5", correct: false },
        { option: "13/5", correct: false }
      ],
      difficult: 3
    },

    {
      question: "(1/6)x - (1/4) = (1/12)",
      answers: [
        { option: "2", correct: true },
        { option: "1/2", correct: false },
        { option: "3", correct: false },
        { option: "4", correct: false }
      ],
      difficult: 3
    },

    {
      question: "(2/3)x + (1/4) = 5",
      answers: [
        { option: "19/12", correct: true },
        { option: "9/8", correct: false },
        { option: "7/6", correct: false },
        { option: "3/2", correct: false }
      ],
      difficult: 3
    },
  
    {
      question: "(3/5)y - (2/5) = 7",
      answers: [
        { option: "47/15", correct: true },
        { option: "19/5", correct: false },
        { option: "13/4", correct: false },
        { option: "11/3", correct: false }
      ],
      difficult: 3
    },
  
    {
      question: "(1/2)a + (2/3) = 4",
      answers: [
        { option: "5/3", correct: true },
        { option: "7/4", correct: false },
        { option: "8/5", correct: false },
        { option: "10/6", correct: false }
      ],
      difficult: 3
    },
  
    {
      question: "(2/7)x - (3/4) = 2",
      answers: [
        { option: "35/14", correct: true },
        { option: "17/7", correct: false },
        { option: "19/8", correct: false },
        { option: "23/10", correct: false }
      ],
      difficult: 3
    },
  
    {
      question: "(4/9)y + (1/3) = 10",
      answers: [
        { option: "25/6", correct: true },
        { option: "29/6", correct: false },
        { option: "31/6", correct: false },
        { option: "37/6", correct: false }
      ],
      difficult: 3
    },
  
    {
      question: "(5/8)x - (1/2) = 3",
      answers: [
        { option: "25/8", correct: true },
        { option: "21/8", correct: false },
        { option: "19/8", correct: false },
        { option: "27/8", correct: false }
      ],
      difficult: 3
    },
  
    {
      question: "(3/10)x + (1/5) = 2",
      answers: [
        { option: "4/5", correct: true },
        { option: "7/10", correct: false },
        { option: "1/2", correct: false },
        { option: "3/4", correct: false }
      ],
      difficult: 3
    },
  
    {
      question: "(2/5)y - (1/3) = 1",
      answers: [
        { option: "11/15", correct: true },
        { option: "7/10", correct: false },
        { option: "3/5", correct: false },
        { option: "5/6", correct: false }
      ],
      difficult: 3
    }
];