const question = document.querySelector("#question");
const answers = document.querySelector(".answers");
const spnQtd = document.querySelector(".spnQtd");
const textFinish = document.querySelector(".finish span");
const h2 = document.querySelector(".title");
const content = document.querySelector(".content");
const contentFinish = document.querySelector(".finish");
const btnRestart = document.querySelector(".finish button");
const reiniciar = document.querySelector(".reiniciar");
var timer = document.querySelector("#time");
const acertos = document.querySelector(".respostas");
const trues = document.querySelector('.true'); 
var watch;
var temporizador = -1;
var tempoPecorrido = document.querySelector(".temporizador");

import questions from "./questionario.js";

function embaralhar(array) {
    var indice_atual = array.length, valor_temporario, indice_aleatorio;
  
    while (0 !== indice_atual) {
  
        indice_aleatorio = Math.floor(Math.random() * indice_atual);
        indice_atual -= 1;
  
        valor_temporario = array[indice_atual];
        array[indice_atual] = array[indice_aleatorio];
        array[indice_aleatorio] = valor_temporario;
    }
  
  return array;
};

let currentIndex = 0;
let questionsCorrect = 0;

var questionsDiff1 = [];
var questionsDiff2 = [];
var questionsDiff3 = [];

var IndexDiff1 = [];
var IndexDiff2 = [];
var IndexDiff3 = [];

var questoesResolvidas = []
var respostas = []

questions.filter( (question) => {

  if(question.difficult == 1){
    questionsDiff1.push(question);

  } else if(question.difficult == 2){
    questionsDiff2.push(question);

  } else{
    questionsDiff3.push(question);
  }

});
  
function EmbaralharQuestions(questions, index, qtd){

  for(var i = 0 ; index.length != qtd ; i++){

    var min = Math.ceil(0);
    var max = Math.floor(questions.length);
  
  
    var key = Math.floor(Math.random() * (max - min) + min);
  
    if(!(index.includes(key))){
      index.push(key);
    }  
  }
}

EmbaralharQuestions(questionsDiff1, IndexDiff1, 3);
EmbaralharQuestions(questionsDiff2, IndexDiff2, 2);
EmbaralharQuestions(questionsDiff3, IndexDiff3, 1);

var QuestoesQuiz = [];

for(let i  = 0; i < IndexDiff1.length ; i++){
    QuestoesQuiz.push(questionsDiff1[IndexDiff1[i]]);
}

for(let i = 0; i < IndexDiff2.length ; i++){
    QuestoesQuiz.push(questionsDiff2[IndexDiff2[i]]);
}

for(let i = 0; i < IndexDiff3.length ; i++){
    QuestoesQuiz.push(questionsDiff3[IndexDiff3[i]]);
}

function tempo(){
  var segundos = 0;
  var minutos = 0; 

  watch = setInterval(() => {

    if(segundos < 10 ){
      timer.innerHTML = "<span id='time'><i class='bx bx-stopwatch'></i>0"+minutos+":0"+ segundos +"</span>";
      segundos++

    } else if(segundos < 60) {
      timer.innerHTML = "<span id='time'><i class='bx bx-stopwatch'></i>0"+minutos+":"+ segundos +"</span>";
      segundos++

    }else if(segundos == 60){
      minutos += segundos / 60
      segundos = 0 

      if(segundos == 0 ){
        timer.innerHTML = "<span id='time'><i class='bx bx-stopwatch'></i>0"+ minutos +":0"+ segundos + "</span>";
        segundos++
      }
      else if(minutos < 10) {
        timer.innerHTML = "<span id='time'><i class='bx bx-stopwatch'></i>0"+ minutos +":"+ segundos + "</span>";
        segundos++
      } else {
        timer.innerHTML = "<span id='time'><i class='bx bx-stopwatch'></i>"+minutos+":"+ segundos +"</span>";
        segundos++
      }
    }

    temporizador += 1
  }, 1000)
}

reiniciar.onclick = () => {
  content.style.display = "flex";
  contentFinish.style.display = "none";

  currentIndex = 0;
  questionsCorrect = 0;
  
  IndexDiff1 = [];
  IndexDiff2 = [];
  IndexDiff3 = [];
  
  questoesResolvidas = []
  respostas = []

  QuestoesQuiz = [];

  embaralhar(questionsDiff1);
  embaralhar(questionsDiff2);
  embaralhar(questionsDiff3);

  EmbaralharQuestions(questionsDiff1, IndexDiff1, 3);
  EmbaralharQuestions(questionsDiff2, IndexDiff2, 2);
  EmbaralharQuestions(questionsDiff3, IndexDiff3, 1);

  for(let i  = 0; i < IndexDiff1.length ; i++){
      QuestoesQuiz.push(questionsDiff1[IndexDiff1[i]]);
  }
  
  for(let i = 0; i < IndexDiff2.length ; i++){
      QuestoesQuiz.push(questionsDiff2[IndexDiff2[i]]);
  }
  
  for(let i = 0; i < IndexDiff3.length ; i++){
      QuestoesQuiz.push(questionsDiff3[IndexDiff3[i]]);
  }

  embaralhar(QuestoesQuiz);
  
  for( let i = 0 ; i < QuestoesQuiz.length ; i++){
      embaralhar(QuestoesQuiz[i].answers);
  }

  clearInterval(watch);
  temporizador = -1;
  timer.innerHTML = "<span id='time'><i class='bx bx-stopwatch' ></i>00:00 </span>"
  tempo();
  loadQuestion();

  section.remove();
  clock.remove();
}

btnRestart.onclick = () => {

    content.style.display = "flex";
    contentFinish.style.display = "none";

    currentIndex = 0;
    questionsCorrect = 0;

    questoesResolvidas = []
    respostas = []

    embaralhar(questionsDiff1);
    embaralhar(questionsDiff2);
    embaralhar(questionsDiff3);

    IndexDiff1 = [];
    IndexDiff2 = [];
    IndexDiff3 = [];

    EmbaralharQuestions(questionsDiff1, IndexDiff1, 3);
    EmbaralharQuestions(questionsDiff2, IndexDiff2, 2);
    EmbaralharQuestions(questionsDiff3, IndexDiff3, 1);

    QuestoesQuiz = [];

    for(let i  = 0; i < IndexDiff1.length ; i++){
        QuestoesQuiz.push(questionsDiff1[IndexDiff1[i]]);
    }
    
    for(let i = 0; i < IndexDiff2.length ; i++){
        QuestoesQuiz.push(questionsDiff2[IndexDiff2[i]]);
    }
    
    for(let i = 0; i < IndexDiff3.length ; i++){
        QuestoesQuiz.push(questionsDiff3[IndexDiff3[i]]);
    }

    embaralhar(QuestoesQuiz);
    
    for( let i = 0 ; i < QuestoesQuiz.length ; i++){
        embaralhar(QuestoesQuiz[i].answers);
    }

    clearInterval(watch);
    temporizador = -1;
    timer.innerHTML = "<span id='time'><i class='bx bx-stopwatch' ></i>00:00 </span>"
    tempo();
    loadQuestion();

    section.remove();
    clock.remove();
};


function nextQuestion(e) {

  questoesResolvidas.push(e.target.parentElement.parentElement.parentElement.childNodes.item(5).childNodes.item(0).nodeValue);
  respostas.push(e.target.getAttribute("data-correct"));

  if (e.target.getAttribute("data-correct") === "true") {
    questionsCorrect++;
  }

  if (currentIndex <= 4) {
    currentIndex++;
    loadQuestion();

  } else {
    finish();
  }

}

var section;
var clock;

function finish() {

  textFinish.innerHTML = `Você acertou ${questionsCorrect} de 6, confira abaixo quais você acertou e errou!`;
  content.style.display = "none";
  contentFinish.style.display = "flex"; 
  contentFinish.style.gap = "30px";
  contentFinish.style.width = "100%";

  clock = document.createTextNode("Tempo percorrido: " + temporizador + " segundos");
  tempoPecorrido.appendChild(clock);

  section = document.createElement('section');
  var i = 0;

  questoesResolvidas.forEach(questao => {

    var div = document.createElement('div');

    if(respostas[i] === "true"){

      div.append(document.createTextNode(questao));
      div.setAttribute("class" , "true");

      section.appendChild(div);
      acertos.appendChild(section);

    } else {

      div.append(document.createTextNode(questao));
      div.setAttribute("class" , "false");

      section.appendChild(div);
      acertos.appendChild(section);
      
    }

    acertos.style.textAlign= 'center';
    i++

  });
}

function loadQuestion() {

  var index = currentIndex + 1;

  spnQtd.innerHTML = `${currentIndex + 1}/6 </span>`;
  const item = QuestoesQuiz[currentIndex];
  answers.innerHTML = "";
  question.innerHTML = item.question;

  item.answers.forEach((answer) => {
    const div = document.createElement("div");

    div.innerHTML = `
    <button class="answer button" data-correct="${answer.correct}">
      ${answer.option}
    </button>
    `;

    answers.appendChild(div);
    
  });

  document.querySelectorAll(".answer").forEach((item) => {
    item.addEventListener("click", nextQuestion);
  });

}

loadQuestion();
tempo();